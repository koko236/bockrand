



import '../styles/index.scss';
import { AudioWave, WaveViewTxt, WaveView } from './AudioWave.js';
// import {TweenMax } from 'gsap';


// let waveWrapper = document.querySelector('.wave');
// let wave1 = new WaveViewTxt(this.audioWave);
// let wave2 = new WaveView(this.audioWave, waveWrapper);
// let waves = [];
class App {

    constructor () {
        this.state = {
            isPaused: true
        };
        this.enterWebsite = this.enterWebsite.bind(this);
    }

    init () {
        this.audioWave = new AudioWave();
        console.log("INIT");
        this.waves = [...document.querySelectorAll('h2')].map(h2 => {
            let wave = new WaveView(this.audioWave, h2);
            let canvas = wave.getDom();
            h2.appendChild(canvas);
            console.log("WAVE", wave);
            return wave;
        });
        
        this._update = this._update.bind(this);
        document.querySelector('#btnStart1').addEventListener ('click', () => this.playSound('./public/dvorak.mp3'));
        document.querySelector('#btnStart2').addEventListener ('click', () => this.playSound('./public/madonna.mp3'));
        document.querySelector('#btnStart3').addEventListener ('click', () => this.playSound('./public/j5.mp3'));
        document.querySelector('#btnStart4').addEventListener ('click', () => this.playSound('./public/beyonce.mp3'));
        
        document.querySelector('.introBtnEnter').addEventListener ('click', this.enterWebsite);

        setTimeout(this.onResize.bind(this), 500);
        // this.playSound('./public/j5.mp3');
        window.addEventListener('resize', this.onResize.bind(this));

    }

    enterWebsite () {
        let intro = document.querySelector(".introWrapper");
        this.playSound('./public/madonna.mp3');
        document.querySelector('.introBtnEnter').removeEventListener('click', this.enterWebsite);
        intro.remove();
    }

    playSound (url) {
        this.audioWave.setSrc(url);
        this.play();
    }

    play () {
        if (this.state.isPaused) {
            this.state.isPaused = false;
            this._update();
        }
    }

    pause () {
        this.state.isPaused = true;
    }

    _update () {
        if (this.state.isPaused) { return; }
        this.audioWave.update();
        this.waves.forEach(wave => wave.update(this.audioWave.getAudioData()));
        requestAnimationFrame(this._update);
    }

    onResize () {
        let mainContentRect = document.querySelector('body').getBoundingClientRect();
        console.log("RESIZE");
        this.waves.forEach(wave => {
            wave.setSize(window.innerWidth, 200);
            // wave.setSize(wave.clientWidth, wave.clientHeight);
            console.log("RESIZE", mainContentRect, -mainContentRect.left+"px");
            wave.getDom().style.marginLeft = -mainContentRect.left+"px";
        });
    }
}


let app = new App();
app.init();

// [...document.querySelectorAll('h2')].forEach((elm) => {
    
//     let wave = new WaveView();
//     waves.push 
// })