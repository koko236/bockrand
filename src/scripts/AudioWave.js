



class AudioWave {

    constructor () {
        // this.views = [];
        // this.maxViews = 3;
        // this.autoplay = true;
        this.audioEL = null;
        this.analyzer = null;
    }

    initAudioElement (src) {

        console.log("INIT  AUDIO");

        if (!this.audioEl) {
            this.audioEl = new Audio();
            this.audioEl.controls = false;
            this.audioEl.crossOrigin = "anonymous";
            this.audioEl.loop = true;
            this.audioEl.autoplay = true;
        }
        this.audioEl.src = src;
        this.audioEl.volume = 0.2;
        document.body.appendChild(this.audioEl);
        return this.audioEl;
    }

    setSrc (url) {
        this.initAudioElement(url);
        if (!this.analyzer)  {
            this.analyzer = new AudioAnalyzer(this.audioEl, 64);
        }
        /// NOTE: Does not work.
        // try {
        //     this.audioEl.play();
        // } catch (e) {
        //     console.warn("PLAY ERROR", e);
        // }
    }

    update () {
        this._audioData = this.analyzer.getAudioData();
        // this.views.forEach(view)
    }
    
    getAudioData () {
        return this._audioData;
    }

    // addWaveView (waveView) {
    //     this.views.push(waveView);
    // }

}

class AudioAnalyzer {

    constructor (audioEl, resolution = 3) {
        console.log("K1");
        this.fftSize = 128;
        if (!window.AudioContext) {
            this.fakeData = Array.apply(null, Array(this.fftSize)).map(Number.prototype.valueOf,0);
        } else {
            this.ctx = new AudioContext();
            console.log("K2");
            this.audio = audioEl;
            this.audioSrc = this.ctx.createMediaElementSource(this.audio);
            this.analyser = this.ctx.createAnalyser();
            this.audioData = [];
            this.resolution = resolution;
            this.analyser.fftSize = this.fftSize;
    
            // Connect the MediaElementSource with the analyser
            this.audioSrc.connect(this.analyser);
            this.audioSrc.connect(this.ctx.destination);
    
            // FrequencyBinCount tells how many values are received from the analyser
            this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
            console.log("FREQ BIN", this.frequencyData);
        }
    }

    getFrequencyData () {
        this.analyser.getByteFrequencyData(this.frequencyData);
        return this.frequencyData;
    }

    getAudioData () {
        if(!this.ctx) {
            return this.fakeData;
        }
        this.analyser.getByteFrequencyData(this.frequencyData);

        // Split array into 3
        var frequencyArray = this.splitFrenquencyArray(this.frequencyData, this.resolution);

        // Make average of frenquency array entries
        for (var i = 0; i < frequencyArray.length; i++) {
            var average = 0;
            for (var j = 0; j < frequencyArray[i].length; j++) {
                average += frequencyArray[i][j];
            }
            this.audioData[i] = average / frequencyArray[i].length;
        }
        return this.audioData;
    }

    splitFrenquencyArray (arr, n) {

        var tab = Object.keys(arr).map(key =>  {
            return arr[key];
        });

        var len = tab.length,
            result = [],
            i = 0;
        
        while (i < len) {
            var size = Math.ceil((len - i) / n--);
            result.push(tab.slice(i, i + size));
            i += size;
        }

        return result;
    }
}

class WaveViewTxt {
    
    constructor (dataSrc) {
        this.dataSrc = dataSrc;
    }

    update () {
        let info = this.dataSrc.getAudioData();
        console.log("RENDER WAVETEXT", info);
    }
}

class WaveView {

    constructor () {

        this.canvas = document.createElement('canvas');
        this.canvas.className = "wave";
        this.ctx = this.canvas.getContext('2d');
        this.props = {};
        this.props.sinT = 0;
        this.props.cycleSpeed = 1000;
        this.props.cycleWidth = 20;
        this.props.sinAmp = 0;
        this.color1 = {r: 128, g: 128, b: 255};
        this.color2 = {r: 255, g: 66, b: 66};
    }

    setSize (w, h) {
        this.canvas.width = w;
        this.canvas.height = h;
    }

    getDom () {
        return this.canvas;
    }

    copyCanvasContent (targetCanvas) {
        let ctx = targetCanvas.getContext('2d');
        ctx.drawImage(this.canvas, 0, 0);
    }

    update (data) {
        // let data = this.dataSrc.getAudioData();
        // let data = this.dataSrc.analyzer.getAudioData(); /// bez cache analizy.
        let normData = FlatRock.transform(data);
        // return;
        let r = {x: 0, y: 0, width: this.canvas.width, height: this.canvas.height};
        this.ctx.clearRect(0, 0, r.width, r.height);
        let segmentWidth = r.width / data.length;

        // this.ctx.moveTo(r.x, r.height);
        let x = 0;
        let level, invLevel, y, tint;
        let shear = 0;

        let sinTimeFactor = Math.PI * 2 / this.props.cycleSpeed;
        let sinOffsetFactor = Math.PI * 2 / this.props.cycleWidth;
        for (let i = 0, l = normData.length; i < l; i++) {
            let base = Math.sin(sinTimeFactor * this.props.sinT + i * sinOffsetFactor) * this.props.sinAmp;
            level = normData[i];
            invLevel = 1 - level;
            tint = (invLevel * this.color1.r + level * this.color2.r) << 16 | 
            (invLevel * this.color1.g + level * this.color2.g) << 8 | 
            (invLevel * this.color1.b + level * this.color2.b); 
            // level * level * 255 << 8 | level * 255;
            this.ctx.fillStyle = '#' + tint.toString(16);
            y = 2 + level * 90;
            // this.ctx.fillRect(x, 0, segmentWidth, 10 + level * r.height);
            this.ctx.beginPath();
            this.ctx.moveTo(x - y * shear, 100 + base - y);
            this.ctx.lineTo(x - y * shear + segmentWidth * .5, 100 + base - y);
            this.ctx.lineTo(x + y * shear + segmentWidth * .5, 100 + base + y);
            this.ctx.lineTo(x + y * shear, 100 + base + y);
            this.ctx.closePath();
            this.ctx.fill();
            x += segmentWidth;
            this.props.sinT += 1;
        }
    }
    
    
    update2 () {
        let data = this.dataSrc.getAudioData();
        let normData = FlatRock.transform(data);
        
        let r = {x: 0, y: 0, width: this.canvas.width, height: this.canvas.height};
        this.ctx.clearRect(0, 0, r.width, r.height);
        this.ctx.fillStyle = '#';
        
        let segmentWidth = r.width / data.length;
        
        // this.ctx.moveTo(r.x, r.height);
        let x = 0;
        let level, y;
        let shear = 0;

        let sinTimeFactor = Math.PI * 2 / this.props.cycleSpeed;
        let sinOffsetFactor = Math.PI * 2 / this.props.cycleWidth;
        for (let i = 0, l = normData.length; i < l; i++) {
            let base = Math.sin(sinTimeFactor * this.props.sinT + i * sinOffsetFactor) * this.props.sinAmp;
            level = normData[i];
            y = 2 + level * 90;
            // this.ctx.fillRect(x, 0, segmentWidth, 10 + level * r.height);
            this.ctx.beginPath();
            this.ctx.moveTo(x - y * shear, 110 + base - y);
            this.ctx.lineTo(x - y * shear + segmentWidth * .2, 110 + base - y);
            this.ctx.lineTo(x + y * shear + segmentWidth * .2, 100 + base - y);
            this.ctx.lineTo(x + y * shear, 100 + base - y);
            this.ctx.closePath();
            this.ctx.fill();
            x += segmentWidth;
            this.props.sinT += 1;
            // console.log(sinTimeFactor * this.props.sinT);
        }
    }

}


function halveLower (p) {
    return 0.5 + p * 0.5;
}

function getNormalizer(max) {
    return v => v / max;
}

function min10 (val) {
    return 10 + val/ 255 * 245;
}

let normalize255 = getNormalizer(255);

class FlatRock {

    static transform (data) {
        let out = [];
        let p;
        for (let i = 0, l = data.length; i < l; i++) {
            p = i / l;
            out[i] = halveLower(p) * normalize255(data[i]);
        }
        return out;
    }
}

export { WaveView, WaveViewTxt, AudioWave };